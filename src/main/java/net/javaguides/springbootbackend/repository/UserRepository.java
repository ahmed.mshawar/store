package net.javaguides.springbootbackend.repository;

import net.javaguides.springbootbackend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {
}
